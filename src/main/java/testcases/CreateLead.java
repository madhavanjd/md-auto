package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ProjectSpecificMethods;
import pages.LoginPage;

public class CreateLead extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setFile() {
		excelFileName = "CreateLead";

	}
	
	@Test(dataProvider="fetchData")
	public void runCreateLead(String username, String password, String fName,String lName,String company) throws InterruptedException {
		
		new LoginPage(driver)
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfaLink()
		.clickLeadsLink()
		.clickCreateLeadLink()
		.enterFirstName(fName)
		.enterLastName(lName)
		.enterCompanyName(company)
		.clickCreateLead()
		.verifyFirstName();
		

	}

}
