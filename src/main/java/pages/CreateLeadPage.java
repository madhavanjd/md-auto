package pages;

import org.openqa.selenium.chrome.ChromeDriver;

import base.ProjectSpecificMethods;

public class CreateLeadPage extends ProjectSpecificMethods {

	public CreateLeadPage(ChromeDriver driver) {
		this.driver = driver;
		
	}
	
	public CreateLeadPage enterFirstName(String firstName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
		return this;

	}
	
	public CreateLeadPage enterLastName(String lastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
		return this;

	}
	
	public CreateLeadPage enterCompanyName(String company) {
		driver.findElementById("createLeadForm_companyName").sendKeys(company);
		return this;

	}
	
	public ViewLeadPage clickCreateLead() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage(driver);

	}
	
}
