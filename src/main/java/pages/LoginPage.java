package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import base.ProjectSpecificMethods;

public class LoginPage extends ProjectSpecificMethods {
	
	public LoginPage(ChromeDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	@FindBy(how=How.CLASS_NAME , using="inputLogin") WebElement eleUsername;
	
public LoginPage enterUsername(String username) throws InterruptedException {
		
		//eleUsername.get(0).sendKeys(username);
		eleUsername.sendKeys(username);
		
		//driver.findElementById("username").sendKeys(username);
		//Thread.sleep(5000);
		return this;

	}
	
	//AND Condition
	/*
	 * @FindBys( {
	 * 
	 * @FindBy(how=How.CLASS_NAME,using="decorativeSubmit"),
	 * 
	 * @FindBy(how=How.XPATH,using="//input[@type='text123']") } ) WebElement
	 * eleUsername;
	 */
	
	//OR condition
	/*
	 * @FindAll( {
	 * 
	 * @FindBy(how=How.CLASS_NAME,using="decorativeSubmit"),
	 * 
	 * @FindBy(how=How.XPATH,using="//input[@type='text123']") } ) WebElement
	 * eleUsername;
	 * 
	 */
	
	
	//List<WebElement> eleUsername;
		
	//action+elementname
	
	
	@CacheLookup
	@FindBy(how=How.ID,using="password") WebElement elePassword;
	
	public LoginPage enterPassword(String password) {
		elePassword.sendKeys(password);
		//driver.findElementById("password").sendKeys(password);
		
		return this;
	}
	
	@CacheLookup
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	public HomePage clickLogin() {
		eleLogin.click();
		//driver.findElementByClassName("decorativeSubmit").click();
		return new HomePage(driver);

	}
	
	public LoginPage clickLoginForInvalidData() {
		eleLogin.click();
		return this;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
